package TrabajoPracticoLaCalculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Ventana extends JFrame {


    public Ventana() {

        setTitle("CALCULADORA");
        setSize(450, 600);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        iniciar();
        setVisible(true);
    }

    JPanel PPrincipal;
    JPanel PBotones;
    JTextField pantalla;
    JButton btn[];
    double op1 = 0, op2 = 0;
    String operacion = "";
    boolean nueva = true;

    public static void main(String args[]) {
        Ventana ventana = new Ventana();
    }

    private void agregarBtns() {

        btn = new JButton[20];

        btn[0] = new JButton("LIMPIAR");
        btn[1] = new JButton("");
        btn[2] = new JButton("");
        btn[3] = new JButton("");
        btn[4] = new JButton("7");
        btn[5] = new JButton("8");
        btn[6] = new JButton("9");
        btn[7] = new JButton("/");
        btn[8] = new JButton("4");
        btn[9] = new JButton("5");
        btn[10] = new JButton("6");
        btn[11] = new JButton("*");
        btn[12] = new JButton("1");
        btn[13] = new JButton("2");
        btn[14] = new JButton("3");
        btn[15] = new JButton("-");
        btn[16] = new JButton("0");
        btn[17] = new JButton(".");
        btn[18] = new JButton("=");
        btn[19] = new JButton("+");

        for (int i = 0; i < 20; i++) {
            PBotones.add(btn[i]);
        }

        btn[19].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (op1 != 0)
                        op1 = op1 + Double.parseDouble(pantalla.getText());
                    else
                        op1 = Double.parseDouble(pantalla.getText());
                    operacion = "suma";
                    pantalla.setText("");
                } catch (Exception err) {
                }
            }
        });
        btn[15].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (op1 != 0)
                        op1 = op1 - Double.parseDouble(pantalla.getText());
                    else
                        op1 = Double.parseDouble(pantalla.getText());
                    operacion = "resta";
                    pantalla.setText("");
                } catch (Exception err) {
                }
            }
        });
        btn[11].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (op1 != 0)
                        op1 = op1 * Double.parseDouble(pantalla.getText());
                    else
                        op1 = Double.parseDouble(pantalla.getText());
                    operacion = "multiplicacion";
                    pantalla.setText("");
                } catch (Exception err) {
                }
            }
        });
        btn[7].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (op1 != 0)
                        op1 = op1 / Double.parseDouble(pantalla.getText());
                    else
                        op1 = Double.parseDouble(pantalla.getText());
                    operacion = "division";
                    pantalla.setText("");
                } catch (Exception err) {
                }
            }
        });


        btn[4].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "7");
            }
        });
        btn[5].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "8");
            }
        });
        btn[6].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "9");
            }
        });
        btn[8].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "4");
            }
        });
        btn[9].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "5");
            }
        });
        btn[10].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "6");
            }
        });
        btn[12].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "1");
            }
        });
        btn[13].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "2");
            }
        });
        btn[14].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "3");
            }
        });
        btn[16].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + "0");
            }
        });
        btn[17].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (nueva) {
                    pantalla.setText("");
                    nueva = false;
                }
                pantalla.setText(pantalla.getText() + ".");
            }
        });

        btn[18].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    op2 = Double.parseDouble(pantalla.getText());
                } catch (Exception err) {
                }
                if (operacion.equals("suma")) {
                    double res = op1 + op2;
                    pantalla.setText(String.valueOf(res));
                    op1 = op2 = 0;
                    operacion = "";
                } else if (operacion.equals("resta")) {
                    double res = op1 - op2;
                    pantalla.setText(String.valueOf(res));
                    op1 = op2 = 0;
                    operacion = "";
                } else if (operacion.equals("multiplicacion")) {
                    double res = op1 * op2;
                    pantalla.setText(String.valueOf(res));
                    op1 = op2 = 0;
                    operacion = "";
                } else if (operacion.equals("division")) {
                    double res = op1 / op2;
                    pantalla.setText(String.valueOf(res));
                    op1 = op2 = 0;
                    operacion = "";
                }
                nueva = true;
            }
        });


        btn[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                pantalla.setText("");
                op1 = op2 = 0;
                operacion = "";
            }
        });
    }

    private void iniciar() {

        PPrincipal = new JPanel();
        PPrincipal.setLayout(new BorderLayout());

        pantalla = new JTextField();
        PPrincipal.add("North", pantalla);

        PBotones = new JPanel();

        PBotones.setLayout(new GridLayout(5, 4, 8, 8));
        agregarBtns();
        PPrincipal.add("Center", PBotones);
        getContentPane().add(PPrincipal);
    }
}
