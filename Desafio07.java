package Projects;

public class Desafio07 {

	public static void main(String[] args) {
	 
	double a,b,c,d,e,f,g;
	a=Math.sin(1);
	b=Math.cos(0.5);
	c=Math.tan(1);
	d=Math.atan(2);
	e=Math.atan2(1,2);
	f=Math.exp(2);
	g=Math.log(3);
	
	System.out.println("Seno de 1: "+a);
	System.out.println("Coseno de 0.5: "+b);	
	System.out.println("Tangente de 1: "+c);	
	System.out.println("ArcoTangente de 2: "+d);
	System.out.println("ArcoTangente2 del punto 1.2: "+e);	
	System.out.println("2 exponente de e: "+ f);		
	System.out.println("logaritmo natural de 3: "+g);
	System.out.println("Numero PI: " +Math.PI);	
	System.out.println("Numero de Euler: " +Math.E);	
	}

}
