package DesafioIndividual6;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class interfaz {

    public static void main(String[] args){

        MyJFrame ventana = new MyJFrame ();

    }
}
class MyJFrame extends JFrame {



    public MyJFrame() {
        setSize(600, 500);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocation(50, 50);
        setVisible(true);
        Panel panel = new Panel();
        add(panel);
        setTitle("Desafio 06");
        addMouseListener(new Panel());

    }

}


    class Panel extends JPanel implements MouseListener {

        @Override
        public void setBackground(Color bg) {
            super.setBackground(Color.GRAY);
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            System.out.println("Se produjo un evento: Click en el panel");
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }

        class Foco implements FocusListener{

            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (focusEvent.getSource() == campo1) {
                    System.out.println("El cuadro 1 ganó el foco");
                }else{
                    System.out.println("El cuadro 2 ganó el foco");
                }
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                if (focusEvent.getSource() == campo1) {
                    System.out.println("El cuadro 1 perdio el foco");

                }else{
                    System.out.println("El cuadro 2 perdió el foco");
                }
            }
        }

        public void paintComponent(Graphics g){
           super.paintComponent(g);
            campo1 = new JTextField();
            campo2 = new JTextField();
            campo1.setBounds(120,10,150,30);
            campo2.setBounds(120,80,150,30);

            add(campo1);
            add(campo2);

            tag1 = new JLabel("Cuadro 1:");
            tag2 = new JLabel("Cuadro 2:");

            tag1.setBounds(30,10,150,20);
            tag2.setBounds(30,80,150,20);

            add(tag1);
            add(tag2);

            Foco foco = new Foco();
            campo1.addFocusListener(foco);
            campo2.addFocusListener(foco);
        }

        private JTextField campo1, campo2;
        private JLabel tag1, tag2;

    }




