import java.io.*;
import java.util.HashSet;
import java.util.Set;


public class Banco {

    public static void main(String[] args) {
	// write your code here
        CuentaCorriente cuenta1 = new CuentaCorriente("Pepe", 5000, 123456);
        CuentaCorriente cuenta2 = new CuentaCorriente("Maria", 8000, 7891011);
        CuentaCorriente cuenta3 = new CuentaCorriente("Leo", 6000, 12131415);

        Set<CuentaCorriente> listaCuentas = new HashSet<>();
        listaCuentas.add(cuenta1);
        listaCuentas.add(cuenta2);
        listaCuentas.add(cuenta3);

        Operaciones.Transferir(cuenta1, cuenta2, 7000);

        System.out.println("SALIDA "+listaCuentas.toString());
        System.out.println("\n");

        try {
            ObjectOutputStream flujoSalida = new ObjectOutputStream(new FileOutputStream(
                    "C:\\Users\\PC\\Desktop\\Agustin\\TUP\\2do semestre\\Laboratorio 2\\Banco\\cuentas.dat"));

            flujoSalida.writeObject(listaCuentas);
            flujoSalida.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream flujoEntrada = new ObjectInputStream(new FileInputStream(
                    "C:\\Users\\PC\\Desktop\\Agustin\\TUP\\2do semestre\\Laboratorio 2\\Banco\\cuentas.dat"));

            Set<CuentaCorriente> listaEntrada = (Set<CuentaCorriente>) flujoEntrada.readObject();

            System.out.println("ENTRADA "+ listaEntrada.toString());

            flujoEntrada.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}


