import java.util.Random;

class UsoCuenta {

    public static void main(String[] args) {

      CuentaCorriente cuenta1 = new CuentaCorriente("Pepe",1000);
      CuentaCorriente cuenta2 = new CuentaCorriente("Pepa",1000);

      cuenta1.ingresar(500);
      cuenta2.sacar(600);
      CuentaCorriente.transferencia(cuenta1,cuenta2,250);

        System.out.println(cuenta1.toString());
        System.out.println(cuenta2.toString());
    }
}
class CuentaCorriente {
    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    public CuentaCorriente(String titular, double saldo) {
        Random random = new Random();
        this.numeroCuenta = Math.abs(random.nextLong());
        this.nombreTitular =  titular;
        this.saldo = saldo;
    }


public void ingresar(double plata) {

    if( plata>0 ){
        this.saldo+=plata;

    }
  }

public void sacar(double plata){
    if( plata>0 ){
        this.saldo-=plata;

    }
    }

public static void transferencia (CuentaCorriente salePlata, CuentaCorriente entraPlata, double plata){
        salePlata.saldo -= plata;
        entraPlata.saldo += plata;
}
    public String toString() {
        return "\n nombreTitular = " + nombreTitular+
                "\n saldo = " + saldo +
                "\n numeroCuenta = " + numeroCuenta + "\n";
    }

}