import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner genero = new Scanner(System.in);
        Scanner altura = new Scanner(System.in);
        int pesoideal;
        System.out.println("Ingrese H para hombre y M para mujer");
        String gen = genero.next();
        System.out.println("Ingrese la altura en cm");
        int alt = altura.nextInt();

        if ("M".equals(gen)){
            pesoideal=alt-120;
            System.out.println("Su peso ideal es "+pesoideal+"kg");
        }
        else if ("H".equals(gen)){
            pesoideal=alt-110;
            System.out.println("Su peso ideal es "+pesoideal+"kg");
        }
        else {
            System.out.println("ERROR- Ingrese M o H -");
        }
    }
}
