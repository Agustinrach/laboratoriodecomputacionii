package TrabajoIndividialProcesadorDeTexto;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.*;

class Principal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VentanaPrincipal ventana = new VentanaPrincipal();
    }

}

public class PanelPrincipal extends JPanel{

    private static JTextPane areaTexto;


    public PanelPrincipal() {

        setLayout(new BorderLayout());

        JMenu archivo = new JMenu("Archivo");
        JMenu fuente = new JMenu("Fuente");
        JMenu estilo = new JMenu("Estilo");
        JMenu tamanio = new JMenu("Tamanio");



        JMenuItem abrir = new JMenuItem("Abrir");
        JMenuItem guardar = new JMenuItem("Guardar");


        archivo.add(abrir);
        archivo.add(guardar);


        abrir.addActionListener(new abrirListener());
        guardar.addActionListener(new escuchandoAlGuardar());
        guardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));


        JMenuItem arial = new JMenuItem("Arial");
        JMenuItem courier = new JMenuItem("Courier");
        JMenuItem verdana = new JMenuItem("Verdana");


        fuente.add(arial);
        fuente.add(courier);
        fuente.add(verdana);



        arial.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Arial"));
        courier.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Courier"));
        verdana.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Verdana"));




        JMenuItem negrita = new JMenuItem("Negrita");
        JMenuItem cursiva = new JMenuItem("Cursiva");
        JMenuItem subrayada = new JMenuItem("Subrayado");


        estilo.add(negrita);
        estilo.add(cursiva);
        estilo.add(subrayada);


        negrita.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        negrita.addActionListener(new StyledEditorKit.BoldAction());
        cursiva.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.CTRL_DOWN_MASK));
        cursiva.addActionListener(new StyledEditorKit.ItalicAction());
        subrayada.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
        subrayada.addActionListener(new StyledEditorKit.UnderlineAction());



        ButtonGroup Tamanios = new ButtonGroup();


        JRadioButtonMenuItem tam8 = new JRadioButtonMenuItem("8");
        JRadioButtonMenuItem tam12 = new JRadioButtonMenuItem("12");
        JRadioButtonMenuItem tam16 = new JRadioButtonMenuItem("16");
        JRadioButtonMenuItem tam20 = new JRadioButtonMenuItem("20");
        JRadioButtonMenuItem tam24 = new JRadioButtonMenuItem("24");


        Tamanios.add(tam8);
        Tamanios.add(tam12);
        Tamanios.add(tam16);
        Tamanios.add(tam20);
        Tamanios.add(tam24);


        tamanio.add(tam8);

        tamanio.add(tam12);

        tamanio.add(tam16);

        tamanio.add(tam20);

        tamanio.add(tam24);



        tam8.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 8));

        tam12.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 12));

        tam16.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 16));

        tam20.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 20));

        tam24.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 24));


        JMenuBar barra = new JMenuBar();
        barra.add(archivo);
        barra.add(fuente);
        barra.add(estilo);

        barra.add(tamanio);

        areaTexto = new JTextPane();

        JScrollPane scroll = new JScrollPane(areaTexto);

        add(barra, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);

    }

    private static String rutaGuardada = "";
    private static String rutaGuardadaStyle = "";
    public static boolean guardado = false;

    public static void cargarDatos () {
        try
        {
            JFileChooser elegirArchivo = new JFileChooser();

            elegirArchivo.showOpenDialog(areaTexto);

            String archivo = elegirArchivo.getSelectedFile().getAbsolutePath();

            String archivoSyle = elegirArchivo.getCurrentDirectory() +"\\"+ elegirArchivo.getSelectedFile().getName() + "Style";

            ObjectInputStream leyendoFichero = new ObjectInputStream(new FileInputStream(archivo));
            ObjectInputStream leyendoFicheroStyle = new ObjectInputStream(new FileInputStream(archivoSyle));

            areaTexto.setText((String) leyendoFichero.readObject());
            areaTexto.setStyledDocument((StyledDocument) leyendoFicheroStyle.readObject());

            leyendoFichero.close();
            leyendoFicheroStyle.close();
            guardado = false;
        }
        catch (IOException ioe)
        {
            JOptionPane.showConfirmDialog(null, "Error!", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();

        }
        catch (ClassNotFoundException c)
        {
            c.printStackTrace();
        }

    }

    private class abrirListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            PanelPrincipal.cargarDatos();
        }
    }

    public static void guardarDatos () {
        try
        {
            JFileChooser eArchivo = new JFileChooser();
            eArchivo.showSaveDialog(areaTexto);

            File archivo = (File) eArchivo.getCurrentDirectory();

            String ruta = archivo+"\\"+eArchivo.getSelectedFile().getName();

            String rutaStyle = archivo+"\\"+eArchivo.getSelectedFile().getName()+"Style";

            rutaGuardada = ruta;
            rutaGuardadaStyle = rutaStyle;

            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(ruta) );
            ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaStyle) );

            escribiendoFichero.writeObject(areaTexto.getText());

            escribiendoFicheroStyles.writeObject(areaTexto.getStyledDocument());
            escribiendoFichero.close();
            escribiendoFicheroStyles.close();

            JOptionPane.showConfirmDialog(null, "Guardado!", "Exito", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
            guardado = true;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void guardadoRapido() {
        try
        {
            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(rutaGuardada) );
            ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaGuardadaStyle) );

            escribiendoFichero.writeObject(areaTexto.getText());
            escribiendoFicheroStyles.writeObject(areaTexto.getStyledDocument());

            escribiendoFicheroStyles.close();
            escribiendoFichero.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private class escuchandoAlGuardar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (guardado) {
                PanelPrincipal.guardadoRapido();
            } else {
                PanelPrincipal.guardarDatos();
            }

        }
    }

}


class VentanaPrincipal extends JFrame{
    public VentanaPrincipal() {

        setTitle("Editor de Texto");

        setBounds(600, 100, 400, 400);

        //--------------------PANEL--------------------
        PanelPrincipal editorTexto = new PanelPrincipal();
        add(editorTexto);

        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addWindowListener(new EventosDeVentana());

    }

    private class EventosDeVentana implements WindowListener{

        @Override
        public void windowOpened(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowClosing(WindowEvent e) {
            if (!PanelPrincipal.guardado) {
                int reply = JOptionPane.showConfirmDialog(null, " Guardar?", "Saliendo sin guardar", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    if (PanelPrincipal.guardado) {
                        PanelPrincipal.guardadoRapido();
                    } else {
                        PanelPrincipal.guardarDatos();
                    }
                }
                else {
                    System.exit(0);
                }

            }
        }

        @Override
        public void windowClosed(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowIconified(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowActivated(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            // TODO Auto-generated method stub

        }

    }
}


